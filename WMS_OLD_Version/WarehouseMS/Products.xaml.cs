﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseMS
{
    /// <summary>
    /// Interaction logic for Products.xaml
    /// </summary>
    public partial class Products : Window
    {
        warehouseEntities ctx;

        public Products()
        {
            try
            {
                InitializeComponent();
                ctx = new warehouseEntities(); // ex SystemException
                //lvProducts.ItemsSource = (from p in ctx.Products select p).ToList<Product>();
                ReloadRecords(); // calls the method to refesh the data from the database
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error, could not connect to the database.
            }
        }

        //public UnitOfMeasure UOM { get; set; }
        public enum UnitOfMeasure { Case = 1, Each = 2, Kg = 3, Litre = 4 };


        public void ReloadRecords()
        {
            try
            {
                List<Product> products = ctx.Products.ToList<Product>();
                lvProducts.ItemsSource = products;
                //lvProducts.ItemsSource = (from p in ctx.Products select p).ToList<Product>(); // ex SystemException
                Utils.AutoResizeColumns(lvProducts);
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

        bool AreInputsValid()
        {
            // Collect the problems, then show them at the end:
            List<string> errorsList = new List<string>();
            if (tbDescriptionProduct.Text.Length < 2 || tbDescriptionProduct.Text.Length > 100)
            {
                errorsList.Add("Name must be between 2 and 100 characters");
            }
            if (cbUnitOfMesureProduct == null) // should never happen
            {
                errorsList.Add("Name must select a Unit of Measure");
            }
            if (!Regex.IsMatch(tbSellingPriceProduct.Text, @"^[0-9]{1,6}(\.([0-9]{2})?)$"))
            {
                errorsList.Add("Selling price must be a decimal number");
            }
            if (!Regex.IsMatch(tbCostPriceProduct.Text, @"^[0-9]{1,6}(\.([0-9]{2})?)$"))
            {
                errorsList.Add("Cost price must be a decimal number");
            }
            if (tbVendorIdProduct == null) // should never happen
            {
                errorsList.Add("Cost price must be a number");
            }
            if (currProductImage == null)
            {
                errorsList.Add("You must choose a picture");
            }
            if (errorsList.Count > 0)
            {
                MessageBox.Show(string.Join("\n", errorsList), "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }
        public Product currSelProduct;
        private void btAddUpdateProduct_Click(object sender, RoutedEventArgs e)
        {
            if (!decimal.TryParse(tbSellingPriceProduct.Text, out decimal sellingPrice))
            {
                MessageBox.Show("Selling Price invalid, must be a decimal");
                return;
            }
            if (!decimal.TryParse(tbCostPriceProduct.Text, out decimal costPrice))
            {
                MessageBox.Show("Cost Price invalid, must be a decimal");
                return;
            }
            if (!int.TryParse(tbVendorIdProduct.Text, out int vendorID))
            {
                MessageBox.Show("VendorID invalid");
                return;
            }
            if (!AreInputsValid()) return;
            currSelProduct = lvProducts.SelectedItem as Product;
            if (currSelProduct == null)
                try
                {
                    string Description = tbDescriptionProduct.Text;
                    string UnitOfMeasure = cbUnitOfMesureProduct.Text;
                    decimal SellingPrice = decimal.Parse(tbSellingPriceProduct.Text);
                    decimal CostPrice = decimal.Parse(tbCostPriceProduct.Text);
                    int VendorID = int.Parse(tbVendorIdProduct.Text);
                    BitmapImage bitmap = Utils.ByteArrayToBitmapImage(currProductImage); // ex: SystemException

                    Product products = new Product
                    {
                        Description = Description,
                        UnitOfMeasure = UnitOfMeasure, // FIXME?
                        SellingPrice = SellingPrice,
                        CostPrice = CostPrice,
                        VendorID = VendorID,
                        ProductImage = currProductImage,
                    };
                    ctx.Products.Add(products); // ex SystemException
                    ctx.SaveChanges(); // ex SystemException
                    lvProducts.ItemsSource = (from p in ctx.Products select p).ToList<Product>();
                    Utils.AutoResizeColumns(lvProducts);
                    lvProducts.Items.Refresh();
                    ClearInputs();
                    ReloadRecords();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            else
            {
                try
                {
                    currSelProduct.Description = tbDescriptionProduct.Text;
                    currSelProduct.UnitOfMeasure = cbUnitOfMesureProduct.Text;
                    currSelProduct.SellingPrice = decimal.Parse(tbSellingPriceProduct.Text);
                    currSelProduct.CostPrice = decimal.Parse(tbCostPriceProduct.Text);
                    currSelProduct.VendorID = int.Parse(tbVendorIdProduct.Text);
                    currSelProduct.ProductImage = currProductImage; // ex: SystemException
                    ctx.SaveChanges();
                    lvProducts.ItemsSource = (from p in ctx.Products select p).ToList<Product>();
                    Utils.AutoResizeColumns(lvProducts);
                    lvProducts.Items.Refresh();
                    ClearInputs();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
        private void btDeleteProduct_Click(object sender, RoutedEventArgs e)
        {
            if (lvProducts.SelectedValue == null)
            {
                MessageBox.Show(this, "Please select an item to delete", "Selection error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Product selProduct = (Product)lvProducts.SelectedItem;
            MessageBoxResult result = MessageBox.Show("Do you want to delete: " + selProduct.ToString() + " ?", "Data save", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                ctx.Products.Remove(selProduct);
                ctx.SaveChanges();
                lvProducts.ItemsSource = (from p in ctx.Products select p).ToList<Product>();
            }
        }
        byte[] currProductImage; // currently selected image, null if none
        private void btImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png|All Files (*.*)|*.*";
            //dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == true)
            {
                try
                {
                    currProductImage = File.ReadAllBytes(dlg.FileName); // ex IOException
                    tbImage.Visibility = Visibility.Hidden; // hide text on the button
                    BitmapImage bitmap = Utils.ByteArrayToBitmapImage(currProductImage); // ex: SystemException
                    imagePreview.Source = bitmap;
                }
                catch (Exception ex) when (ex is SystemException || ex is IOException)
                {
                    Console.WriteLine(ex);
                    MessageBox.Show(this, ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
        public void ClearInputs()
        {
            tbDescriptionProduct.Text = "";
            cbUnitOfMesureProduct.SelectedItem = "Each";
            tbSellingPriceProduct.Text = "";
            tbCostPriceProduct.Text = "";
            imagePreview.Source = null;
            btDeleteProduct.IsEnabled = false;
            tbImage.Visibility = Visibility.Visible;
        }
        private void lvProducts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currSelProduct = lvProducts.SelectedItem as Product;
            if (currSelProduct == null)
            {
                btAddUpdateProduct.Content = "Add/Update";
                ClearInputs();
                return;
            }

            else
            {
                try
                {
                    Product updatedProduct = (Product)lvProducts.SelectedItem;
                    tbDescriptionProduct.Text = updatedProduct.Description;
                    cbUnitOfMesureProduct.Text = updatedProduct.UnitOfMeasure;
                    tbSellingPriceProduct.Text = updatedProduct.SellingPrice.ToString();
                    tbCostPriceProduct.Text = updatedProduct.CostPrice.ToString();
                    tbVendorIdProduct.Text = updatedProduct.VendorID.ToString();
                    //btImage.Content = updatedProduct.ProductImage; // ex: SystemException

                    //currProductImage = File.ReadAllBytes(dlg.FileName); // ex IOException
                    tbImage.Visibility = Visibility.Hidden; // hide text on the button
                    BitmapImage bitmap = Utils.ByteArrayToBitmapImage(updatedProduct.ProductImage); // ex: SystemException
                    imagePreview.Source = bitmap;

                    btDeleteProduct.IsEnabled = true;
                    btAddUpdateProduct.Content = "Update";
                }
                catch (SystemException ex)
                {
                    Console.WriteLine(ex);
                    MessageBox.Show(ex.Message, "Loading image failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Do you want to leave?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.No)
            {
                this.Close();
            }
        }

    }
}
