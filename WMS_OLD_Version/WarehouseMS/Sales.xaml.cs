﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseMS
{
    /// <summary>
    /// Interaction logic for Sales.xaml
    /// </summary>
    public partial class Sales : Window
    {
        warehouseEntities ctx;

        public Sales()
        {
            try
            {
                InitializeComponent();
                ctx = new warehouseEntities();
                lvSalesOrder.ItemsSource = (from p in ctx.Sale_Order select p).ToList<Sale_Order>();
                lvSalesOrder.Items.Refresh();
            }
            catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Fatal error: Database connection failed:\n" + ex.Message);
                Environment.Exit(1); // fatal error
            }
        }

        public Customer currSelectedCustomer; // null if adding, non-null if editing
        public Product currSelectedProduct; // null if adding, non-null if editing

        public Sale_Order currSelOrder; // null if adding, non-null if editing

        private void AddEditOrder_Click(object sender, RoutedEventArgs e)
        {
            AddEditSaleOrders dlg = new AddEditSaleOrders();
            dlg.Owner = this;

            if (dlg.ShowDialog() == true)
            {
                Sale_Order currSelOrder = (Sale_Order)lvSalesOrder.SelectedItem;
                currSelectedProduct = dlg.lvProducts.SelectedItem as Product;
                currSelectedCustomer = dlg.lvCustomers.SelectedItem as Customer;
                if (currSelOrder == null)   // If add new sale order
                {
                    try
                    {
                        if (!int.TryParse(dlg.tbQuantitySales.Text, out int SaleQuantity))
                        {
                            MessageBox.Show("Quantity must be an integer");
                            return;
                        }

                        Sale_Order s = new Sale_Order
                        {

                            ProductID = currSelectedProduct.ProductID,
                            CustomersID = currSelectedCustomer.CustomersID,
                            Description = currSelectedProduct.Description,
                            SaleQuantity = SaleQuantity,
                            UnitOfMeasure = currSelectedProduct.UnitOfMeasure,
                            SellingPrice = currSelectedProduct.SellingPrice,
                            ShipMethod = dlg.cbShippingSales.Text,
                            OrderDate = (DateTime)dlg.dpOrderDateSales.SelectedDate,
                            ShipDate = (DateTime)dlg.dpShipDateSales.SelectedDate,
                            OrderStatus = dlg.cbStatusSales.Text
                        };
                        ctx.Sale_Order.Add(s);
                        ctx.SaveChanges();
                        lvSalesOrder.ItemsSource = (from p in ctx.Sale_Order select p).ToList<Sale_Order>();


                    }
                    catch (SystemException ex)
                    {
                        MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
                else    // if edit existing order
                {
                    try
                    {
                        if (!int.TryParse(dlg.tbQuantitySales.Text, out int SaleQuantity))
                        {
                            MessageBox.Show("Quantity must be an integer");
                            return;
                        }

                        currSelOrder.ProductID = currSelectedProduct.ProductID;
                        currSelOrder.CustomersID = currSelectedCustomer.CustomersID;
                        currSelOrder.Description = currSelectedProduct.Description;
                        currSelOrder.SaleQuantity = SaleQuantity;
                        currSelOrder.UnitOfMeasure = currSelectedProduct.UnitOfMeasure;
                        currSelOrder.SellingPrice = currSelectedProduct.SellingPrice;
                        currSelOrder.ShipMethod = dlg.cbShippingSales.Text;
                        currSelOrder.OrderDate = (DateTime)dlg.dpOrderDateSales.SelectedDate;
                        currSelOrder.ShipDate = (DateTime)dlg.dpShipDateSales.SelectedDate;
                        currSelOrder.OrderStatus = dlg.cbStatusSales.Text;

                        ctx.SaveChanges();
                        lvSalesOrder.ItemsSource = (from p in ctx.Sale_Order select p).ToList<Sale_Order>();
                        lvSalesOrder.Items.Refresh();

                    }
                    catch (SystemException ex)
                    {
                        MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }

            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void DeleteOrder_Click(object sender, RoutedEventArgs e)
        {
            Sale_Order currSelOrder = (Sale_Order)lvSalesOrder.SelectedItem;
            if (currSelOrder == null) { return; } // should never happen
            if (MessageBoxResult.OK != MessageBox.Show("Do you want to delete the customer " + currSelOrder + "?\n", "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning))
            { return; } // cancelled
            try
            {

                ctx.Sale_Order.Remove(currSelOrder);
                ctx.SaveChanges();
                lvSalesOrder.ItemsSource = (from p in ctx.Sale_Order select p).ToList<Sale_Order>();
                lvSalesOrder.Items.Refresh();

            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        private void lvSalesOrder_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Sale_Order currSelOrder = (Sale_Order)lvSalesOrder.SelectedItem;

            if (currSelOrder == null)
            {
                return;
            }
            else
            {
                btAddEditOrder.IsEnabled = false;
                btDeleteOrder.IsEnabled = true;
                btEditOrder.IsEnabled = true;
            }
        }

        private void btEditOrder_Click(object sender, RoutedEventArgs e)
        {
            // Sale_Order currSelOrder = (Sale_Order)lvSalesOrder.SelectedItem;
            currSelOrder = lvSalesOrder.SelectedItem as Sale_Order;

            if (currSelOrder == null)
            {
                return;
            }
            else
            {

                AddEditSaleOrders dlg = new AddEditSaleOrders();
                dlg.Owner = this;

                // currSelectedCustomer = dlg.lvCustomers.SelectedItem as Customer;
                // currSelectedProduct = dlg.lvProducts.SelectedItem as Product;
                btDeleteOrder.IsEnabled = true;
                dlg.dpOrderDateSales.SelectedDate = currSelOrder.OrderDate;
                dlg.dpShipDateSales.SelectedDate = currSelOrder.ShipDate;
                dlg.lblOrderIdSales.Content = currSelOrder.SaleOrderID;
                dlg.tbQuantitySales.Text = currSelOrder.SaleQuantity.ToString();
                dlg.cbShippingSales.SelectedItem = currSelOrder.ShipMethod;  // Doesn't work
                dlg.cbStatusSales.SelectedItem = currSelOrder.OrderStatus;  // Doesn't work
                // dlg.tbFromSales = currSelOrder.WarehouseID.ToString();
                // currSelectedCustomer.CustomersID = currSelOrder.CustomersID;
                // dlg.lvCustomers.
                dlg.btPlaceOrderSales.Content = "Save Changes";
                dlg.ShowDialog();
            }
        }

    }
}
