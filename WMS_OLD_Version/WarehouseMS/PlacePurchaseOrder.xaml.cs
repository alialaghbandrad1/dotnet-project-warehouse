﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseMS
{
    /// <summary>
    /// Interaction logic for PlacePurchaseOrder.xaml
    /// </summary>
    public partial class PlacePurchaseOrder : Window
    {
        warehouseEntities ctx;
        public PlacePurchaseOrder()
        {
            InitializeComponent();
            ctx = new warehouseEntities();
            lvProducts.ItemsSource = (from p in ctx.Products select p).ToList<Product>();
            lvVendors.ItemsSource = (from t in ctx.Vendors select t).ToList<Vendor>();
            dpOrderDatePurchase.SelectedDate = DateTime.Today;
            dpShipDatePurchase.SelectedDate = DateTime.Today;
        }

        public Vendor currSelVendor;
        public Product currSelProduct;

        private void btPlaceOrderPurchase_Click(object sender, RoutedEventArgs e)
        {
            currSelVendor = (Vendor)lvVendors.SelectedItem;
            currSelProduct = (Product)lvProducts.SelectedItem;

            DateTime orderDate = dpOrderDatePurchase.SelectedDate.Value;
            int purchaseQuantity = Int32.Parse(tbQuantityPurchase.Text);
            decimal costPerUnit = currSelProduct.CostPrice;
            decimal totalCost = purchaseQuantity * costPerUnit;
            DateTime shippingDate = dpShipDatePurchase.SelectedDate.Value;
            string method = cbShipMethodPurchase.Text;
            int idVendor = currSelVendor.VendorID;
            int idProduct = currSelProduct.ProductID;
            string description = currSelProduct.Description;
            string unitOfMesure = currSelProduct.UnitOfMeasure;
            string status = cbShipStatusPurchase.Text;



            DialogResult = true; // assign result and dissmiss dialog

        }

        private void lvVendors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currSelVendor = (Vendor)lvVendors.SelectedItem;
            if (currSelVendor == null)
            {
                return;
            }
            else
            {
                lvProducts.ItemsSource = (from p in ctx.Products select p).Where(s => s.VendorID == currSelVendor.VendorID).ToList<Product>();
            }
        }

        private void lvProducts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currSelProduct = (Product)lvProducts.SelectedItem;
            if (currSelProduct == null)
            {
                lblQuanityUnitPurchase.Content = "-";
            }
            else
            {
                lblQuanityUnitPurchase.Content = currSelProduct.UnitOfMeasure;
            }
        }

        private void btCancelPurchase_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void tbQuantityPurchase_TextChanged(object sender, TextChangedEventArgs e)
        {
            currSelProduct = (Product)lvProducts.SelectedItem;


            if (currSelProduct == null)
            {
                lblTotalCostPurchase.Content = "...";
            }
            else
            {
                int purchaseQuantity = int.Parse(tbQuantityPurchase.Text);
                decimal costPerUnit = currSelProduct.CostPrice;
                decimal totalCost = purchaseQuantity * costPerUnit;
                lblTotalCostPurchase.Content = totalCost;

            }
        }
    }
}
