﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseMS
{
    /// <summary>
    /// Interaction logic for Customers.xaml
    /// </summary>
    public partial class Customers : Window
    {
        warehouseEntities ctx;
        // List<Customer> customerList = new List<Customer>();
        public Customers()
        {
            try
            {
                InitializeComponent();
                ctx = new warehouseEntities();
                lvCustomers.ItemsSource = (from p in ctx.Customers select p).ToList<Customer>();
            }
            catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Fatal error: Database connection failed:\n" + ex.Message);
                Environment.Exit(1); // fatal error
            }

        }

        public Customer currSelCustomer; // null if adding, non-null if editing

        private void btAddUpdateCustomer_Click(object sender, RoutedEventArgs e)
        {
            currSelCustomer = lvCustomers.SelectedItem as Customer;
            if (!inputValidation()) return;
            if (currSelCustomer == null)
            {

                try
                {
                    Customer c = new Customer
                    {
                        CompanyName = tbNameCompany.Text,
                        ContactName = tbContactNameCustomer.Text,
                        Address = tbAddressCustomer.Text,
                        City = tbCityCustomer.Text,
                        Province = tbProvinceCustomer.Text,
                        Country = tbCountryCustomer.Text,
                        Phone = tbPhoneCustomer.Text
                    };
                    ctx.Customers.Add(c);
                    ctx.SaveChanges();
                    lvCustomers.ItemsSource = (from p in ctx.Customers select p).ToList<Customer>();
                    lvCustomers.Items.Refresh();
                    ClearUserInputs();

                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            else
            {
                try
                {
                    currSelCustomer.CompanyName = tbNameCompany.Text;
                    currSelCustomer.ContactName = tbContactNameCustomer.Text;
                    currSelCustomer.Address = tbAddressCustomer.Text;
                    currSelCustomer.City = tbCityCustomer.Text;
                    currSelCustomer.Province = tbProvinceCustomer.Text;
                    currSelCustomer.Country = tbCountryCustomer.Text;
                    currSelCustomer.Phone = tbPhoneCustomer.Text;
                    ctx.SaveChanges();
                    lvCustomers.ItemsSource = (from p in ctx.Customers select p).ToList<Customer>();
                    lvCustomers.Items.Refresh();
                    ClearUserInputs();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        public void ClearUserInputs()
        {
            lblIdCustomer.Content = "...";
            tbNameCompany.Clear();
            tbContactNameCustomer.Clear();
            tbContactNameCustomer.Clear();
            tbAddressCustomer.Clear();
            tbCityCustomer.Clear();
            tbProvinceCustomer.Clear();
            tbCountryCustomer.Clear();
            tbPhoneCustomer.Clear();
            btDeleteCustomer.IsEnabled = false;
            btCancelCustomer.IsEnabled = true;
        }

        private void btCancelCustomer_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void lvCustomers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currSelCustomer = lvCustomers.SelectedItem as Customer;
            if (currSelCustomer == null)
            {
                ClearUserInputs();
                return;
            }
            else
            {
                Customer c = (Customer)lvCustomers.SelectedItem;
                lblIdCustomer.Content = c.CustomersID;
                btDeleteCustomer.IsEnabled = true;
                btCancelCustomer.IsEnabled = true;
                btAddUpdateCustomer.Content = "Update Customer";
                tbNameCompany.Text = c.CompanyName;
                tbContactNameCustomer.Text = c.ContactName;
                tbAddressCustomer.Text = c.Address;
                tbCityCustomer.Text = c.City;
                tbProvinceCustomer.Text = c.Province;
                tbCountryCustomer.Text = c.Country;
                tbPhoneCustomer.Text = c.Phone;
            }

        }

        private void btDeleteCustomer_Click(object sender, RoutedEventArgs e)
        {
            Customer currSelCustomer = (Customer)lvCustomers.SelectedItem;
            if (currSelCustomer == null) { return; } // should never happen
            if (MessageBoxResult.OK != MessageBox.Show("Do you want to delete the customer " + currSelCustomer + "?\n", "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning))
            { return; } // cancelled
            try
            {
                ctx.Customers.Remove(currSelCustomer);
                ctx.SaveChanges();
                lvCustomers.ItemsSource = (from p in ctx.Customers select p).ToList<Customer>();
                lvCustomers.Items.Refresh();
                ClearUserInputs();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        bool inputValidation()
        {
            List<string> errorsList = new List<string>();
            string name = tbNameCompany.Text;
            string accents = "àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸ";
            if (!Regex.IsMatch(name, @"^[a-zA-Z" + accents + " '-]{1,45}$"))
            {
                errorsList.Add("Name must be between 1 and 45 characters");
            }
            string contact = tbContactNameCustomer.Text;
            if (!Regex.IsMatch(contact, @"^[a-zA-Z" + accents + " '-]{1,45}$"))
            {
                errorsList.Add("Contact Name must be between 1 and 45 characters");
            }
            string phone = tbPhoneCustomer.Text;
            if (!Regex.IsMatch(phone, @"^[1-9]\d{2}-\d{3}-\d{4}$"))
            {
                errorsList.Add("Phone format error. i.e 555-555-5555");
            }
            string address = tbAddressCustomer.Text;
            if (!Regex.IsMatch(address, @"^[a-zA-Z\d" + accents + " #&'-]{1,100}$"))
            {
                errorsList.Add("Address must be between 1 and 100 characters");
            }
            string city = tbCityCustomer.Text;
            if (!Regex.IsMatch(city, @"^[a-zA-Z" + accents + " '-]{1,45}$"))
            {
                errorsList.Add("City must be between 1 and 45 characters");
            }
            string province = tbProvinceCustomer.Text;
            if (!Regex.IsMatch(province, @"^[a-zA-Z" + accents + " '-]{1,45}$"))
            {
                errorsList.Add("Province\\State must be between 1 and 45 characters");
            }
            string country = tbCountryCustomer.Text;
            if (!Regex.IsMatch(country, @"^[a-zA-Z" + accents + " '-]{1,45}$"))
            {
                errorsList.Add("Country must be between 1 and 45 characters");
            }
            if (errorsList.Count > 0)
            {
                MessageBox.Show(string.Join("\n", errorsList), "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

    }
}
