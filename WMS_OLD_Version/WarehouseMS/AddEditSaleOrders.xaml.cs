﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseMS
{
    /// <summary>
    /// Interaction logic for AddEditSaleOrders.xaml
    /// </summary>
    public partial class AddEditSaleOrders : Window
    {
        warehouseEntities ctx;

        public AddEditSaleOrders()
        {
            try
            {
                InitializeComponent();
                ctx = new warehouseEntities();
                lvCustomers.ItemsSource = (from p in ctx.Customers select p).ToList<Customer>();
                lvProducts.ItemsSource = (from p in ctx.Products select p).ToList<Product>();
            }
            catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Fatal error: Database connection failed:\n" + ex.Message);
                Environment.Exit(1); // fatal error
            }
        }

        public Customer currSelectedCustomer; // null if adding, non-null if editing
        public Product currSelectedProduct; // null if adding, non-null if editing


        private void AddEditSaleOrder_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true; // assign result and dissmiss dialog
        }

        private void btCancelSales_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void lvProducts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }
    }
}
