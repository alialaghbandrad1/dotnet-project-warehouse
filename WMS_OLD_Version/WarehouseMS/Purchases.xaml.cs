﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseMS
{
    /// <summary>
    /// Interaction logic for Purchases.xaml
    /// </summary>
    public partial class Purchases : Window
    {
        warehouseEntities ctx;
        public Purchases()
        {
            InitializeComponent();
            ctx = new warehouseEntities();
            lvPurchaseOrder.ItemsSource = (from p in ctx.Purchase_Order select p).ToList<Purchase_Order>();
            Utils.AutoResizeColumns(lvPurchaseOrder);
            lvPurchaseOrder.Items.Refresh();
        }

        private void btCancelPurchase_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public Vendor currSelectedVendor; // null if adding, non-null if editing
        public Product currSelectedProduct; // null if adding, non-null if editing

        private void btAddPurchase_Click(object sender, RoutedEventArgs e)
        {
            PlacePurchaseOrder dlg = new PlacePurchaseOrder();
            dlg.Owner = this;
            if(dlg.ShowDialog() == true)
            {
                try
                {
                    DateTime orderDate = dlg.dpOrderDatePurchase.SelectedDate.Value;
                    int purchaseQuantity = Int32.Parse(dlg.tbQuantityPurchase.Text);
                    decimal costPerUnit = dlg.currSelProduct.CostPrice;
                    decimal totalCost = purchaseQuantity * costPerUnit;
                    DateTime shippingDate = dlg.dpShipDatePurchase.SelectedDate.Value;
                    string method = dlg.cbShipMethodPurchase.Text;
                    int idVendor = dlg.currSelVendor.VendorID;
                    int idProduct = dlg.currSelProduct.ProductID;
                    string description = dlg.currSelProduct.Description;
                    string unitOfMesure = dlg.currSelProduct.UnitOfMeasure;
                    string status = dlg.cbShipStatusPurchase.Text;

                    Purchase_Order newOrder = new Purchase_Order
                    {
                        ProductID = idProduct,
                        VendorID = idVendor,
                        Description = description,
                        PurchaseQuantity = purchaseQuantity,
                        UnitOfMeasure = unitOfMesure,
                        CostPrice = totalCost,
                        ShipMethod = method,
                        PurchaseDate = orderDate,
                        ShipDate = shippingDate,
                        OrderStatus = status
                    };
                    ctx.Purchase_Order.Add(newOrder);
                    ctx.SaveChanges();
                    lvPurchaseOrder.ItemsSource = (from t in ctx.Purchase_Order select t).ToList<Purchase_Order>();
                    Utils.AutoResizeColumns(lvPurchaseOrder);
                    lvPurchaseOrder.Items.Refresh();


                }
                catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
                {
                    Console.WriteLine(ex.StackTrace);
                    MessageBox.Show("Database operation failed:\n" + ex.Message);
                }
            }
        }

        private void btDeletePurchase_Click(object sender, RoutedEventArgs e)
        {
            Purchase_Order currPurchaseOrder = (Purchase_Order)lvPurchaseOrder.SelectedItem;
            if (currPurchaseOrder == null) { return; } // should never happen
            if (MessageBoxResult.OK != MessageBox.Show("Do you want to delete the order?\n " + currPurchaseOrder, "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning))
            { return; } // cancelled
            try
            {

                ctx.Purchase_Order.Remove(currPurchaseOrder);
                ctx.SaveChanges();
                lvPurchaseOrder.ItemsSource = (from p in ctx.Purchase_Order select p).ToList<Purchase_Order>();
                lvPurchaseOrder.Items.Refresh();

            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }
    }
}