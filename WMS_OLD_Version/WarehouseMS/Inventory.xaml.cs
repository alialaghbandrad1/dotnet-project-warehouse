﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseMS
{
    /// <summary>
    /// Interaction logic for Inventory.xaml
    /// </summary>
    public partial class Inventory : Window
    {
        warehouseEntities ctx;
        public Inventory()
        {
            try
            {
                InitializeComponent();
                ctx = new warehouseEntities(); // ex SystemException
                //lvInventory.ItemsSource = (from p in ctx.Inventories select p).ToList<Inventory>();
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error, could not connect to the database.
            }
        }

        private void btAddUpdateInventory_Click(object sender, RoutedEventArgs e)
        {
            if (!int.TryParse(tbProductInventory.Text, out int ProductID))
            {
                MessageBox.Show("Product ID invalid");
                return;
            }
            if (!int.TryParse(tbQuantityInventory.Text, out int Quantity))
            {
                MessageBox.Show("Quantity invalid");
                return;
            }
            if (!int.TryParse(tbWarehouseInventory.Text, out int WarehouseID))
            {
                MessageBox.Show("Warehouse ID invalid");
                return;
            }
            if (!int.TryParse(tbQuantitySold.Text, out int QuantitySold))
            {
                MessageBox.Show("Quantity Sold invalid");
                return;
            }
            if (!AreInputsValid()) return;
            currSelItem = lvInventory.SelectedItem as Inventory;
            if (currSelItem == null)
                try
                {   
                    Inventory inventories = new Inventory
                    {
                        ProductID = ProductID,
                        Quantity = Quantity,
                        WarehouseID = WarehouseID,
                        QuantitySold = QuantitySold,
                    };
                    ctx.Inventories.Add(inventories); // ex SystemException
                    ctx.SaveChanges(); // ex SystemException
                    //lvInventory.ItemsSource = (from I in ctx.Inventories select I).ToList<Inventory>();
                    Utils.AutoResizeColumns(lvInventory);
                    lvInventory.Items.Refresh();
                    ClearInputs();
                    //ReloadRecords();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            else
            {
                try
                {
                    currSelItem.ProductID = int.Parse(tbProductInventory.Text);
                    currSelItem.Quantity = int.Parse(tbQuantityInventory.Text);
                    currSelItem.WarehouseID = int.Parse(tbWarehouseInventory.Text);
                    currSelItem.QuantitySold = int.Parse(tbQuantitySold.Text);
                    ctx.SaveChanges();
                    //lvInventory.ItemsSource = (from I in ctx.Inventories select I).ToList<Inventory>();
                    Utils.AutoResizeColumns(lvInventory);
                    lvInventory.Items.Refresh();
                    ClearInputs();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void btDeleteInventory_Click(object sender, RoutedEventArgs e)
        {
            if (lvInventory.SelectedValue == null)
            {
                MessageBox.Show(this, "Please select an item to delete", "Selection error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Inventory currSelItem = (Inventory)lvInventory.SelectedItem;
            MessageBoxResult result = MessageBox.Show("Do you want to delete: " + currSelItem.ToString() + " ?", "Data save", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                ctx.Inventories.Remove(currSelItem);
                ctx.SaveChanges();
                //lvInventory.ItemsSource = (from I in ctx.Inventories select I).ToList<Inventory>();
            }
        }

        private void btCancelInventory_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Do you want to leave?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.No)
            {
                this.Close();
            }
        }

        public Inventory currSelItem;

        private void lvInventory_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currSelItem = lvInventory.SelectedItem as Inventory;
            if (currSelItem == null)
            {
                btAddUpdateInventory.Content = "Add/Update";
                ClearInputs();
                return;
            }

            else
            {
                try
                {

                    Inventory updateInv = (Inventory)lvInventory.SelectedItem;
                    tbProductInventory.Text = updateInv.ProductID.ToString();
                    tbQuantityInventory.Text = updateInv.Quantity.ToString();
                    tbWarehouseInventory.Text = updateInv.WarehouseID.ToString();
                    tbQuantitySold.Text = updateInv.QuantitySold.ToString();
                    btDeleteInventory.IsEnabled = true;
                    btAddUpdateInventory.Content = "Update";
                }
                catch (SystemException ex)
                {
                    Console.WriteLine(ex);
                    MessageBox.Show(ex.Message, "Loading image failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
        /*
        public void ReloadRecords()
        {
            try
            {
                lvInventory.ItemsSource = (from I in ctx.Inventories select I).ToList<Inventory>(); // ex SystemException
                Utils.AutoResizeColumns(lvInventory);
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        */
        bool AreInputsValid()
        {
            // Collect the problems, then show them at the end:
            List<string> errorsList = new List<string>();
            if (tbProductInventory == null) // should never happen
            {
                errorsList.Add("Product ID must be a whole number");
            }
            if (tbQuantityInventory == null) // should never happen
            {
                errorsList.Add("Quantity must be a whole number");
            }
            if (tbWarehouseInventory == null) // should never happen
            {
                errorsList.Add("Warehouse ID must be a whole number");
            }
            if (tbQuantitySold == null) // should never happen
            {
                errorsList.Add("Quantity must be a whole number");
            }
            if (errorsList.Count > 0)
            {
                MessageBox.Show(string.Join("\n", errorsList), "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        public void ClearInputs()
        {
            tbProductInventory.Text = "";
            tbQuantityInventory.Text = "";
            tbWarehouseInventory.Text = "";
            tbQuantitySold.Text = "";
            btDeleteInventory.IsEnabled = false;
        }
    }
}
