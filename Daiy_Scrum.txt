DAILY SCRUM


2021/04/07 Khaled
1. Done / not done since last scrum:
-	Started the project description
-	Started Trello board
2. To do until next Scrum:
-	finish the description
-	setup the initial project
-	Start to work on XAML windows 
3. Need assistance / figure things out
-	TBD

2021/04/07 Mathew
1. Done / not done since last scrum:
-	Started the design of database
2. To do until next Scrum:
-	Finalize database design after the approval
3. Need assistance / figure things out
-	Use VS and implement database-first method

2021/04/07 Ali
1. Done / not done since last scrum:
-	Started the XAML mock ups
2. To do until next Scrum:
-	Finalize the mock ups after the approval
3. Need assistance / figure things out
-	TBD
 
2021/04/08 Ali
1.	Done since last scrum:
-	Creating database Azure
-	Updating the XAML mock up of sale_order and purchase order
-	Group working on updating database diagram and entities
-	Submitting project proposal

2. To do until next Scrum:
-	Implementing database on Azure and connect to project VS project
-	Improving VS project (setting up the main window with buttons)
-       Start working on VS coding windows of Order and customer

3. Need assistance / figure things out
-	TBD

2021/04/08 Khaled
1. Done / not done since last scrum:
-	Worked on database entities
-	Updated Trello board
2. To do until next Scrum:
-	Start working on VS coding windows of Vendor & Report
-	Implemented vendor class
-	started database-first tutorial before moving forward
 
3. Need assistance / figure things out
-	TBD


2021/04/08 Mathew
1. Done / not done since last scrum:
-	Worked on database entities
-	Worked with team on overall architecture of the database	
-	Updated Trello board

2. To do until next Scrum:
-	Start working on VS coding windows of Product and Inventory
-	Create User Stories
-	Add User Stories to Trello

3. Need assistance / figure things out
-	TBD

2021/04/09 Ali
1.	Done since last scrum:
-	Creating database Azure
-	Updating the XAML mock up of sale_order and purchase order
-	Group working on updating database diagram and entities
-	Submitting project proposal
-	Implementing database on Azure and connect to project VS project

2. To do until next Scrum:

-	Improving VS project (setting up the main window with buttons empty for now. Purchase order in a new window)
-       Start working on VS coding windows of Order (saleorder & purchaseorder) and customer
-	On-hand quantity and reserved quantity in inventory (calculated field)

3. Need assistance / figure things out
-	TBD

2021/04/09 Khaled
1. Done / not done since last scrum:
-	Worked on database entities
-	Updated Trello board
2. To do until next Scrum:
-	Start working on VS coding windows of Vendor & Report
-	continue studying database-first tutorial 
-	On-hand quantity and reserved quantity in inventory (calculated field)

3. Need assistance / figure things out
-	TBD


2021/04/09 Mathew
1. Done / not done since last scrum:	
-	Updated Trello board

2. To do until next Scrum:
-	Start working on VS coding windows of Product and Inventory
-	continue studying database-first tutorial 
-	Updating table of product
-	On-hand quantity and reserved quantity in inventory (calculated field)

3. Need assistance / figure things out
-	TBD


===================================

2021/04/12 Ali

1. Done / not done since last scrum:
1.	Done since last scrum:
-	Added buttons to Main Window
-	Working on Adding Customer on Customer window
-	studied database-first tutorial 

2. To do until next Scrum:

-	Connecting database
-	Fixing Customer database error
-       Start working on VS coding windows of Order (saleorder & purchaseorder) and customer
-	On-hand quantity and reserved quantity in inventory (calculated field)

3. Need assistance / figure things out
-	Question from teacher: How to fix database connection error

2021/04/12 Khaled
1. Done / not done since last scrum:
-	Worked on Vendor window
-	studied database-first tutorial 
2. To do until next Scrum:
-	Continue working on VS coding windows of Vendor & Report
-	On-hand quantity and reserved quantity in inventory (calculated field)

3. Need assistance / figure things out
-	Question from teacher: How to fix database connection error


2021/04/12 Mathew
1. Done / not done since last scrum:	
-	Worked on Product window
-	studied database-first tutorial 
-	Updated Trello Added a page for Product & Main page

2. To do until next Scrum:
-	Continue working on VS coding windows of Product and Inventory
-	Updating table of product
-	On-hand quantity and reserved quantity in inventory (calculated field)
-	Add user stories to Trello

3. Need assistance / figure things out
-	TBD


===================================

2021/04/13 Ali

1. Done / not done since last scrum:
-	Connected database
-	studied database input validation
-	Completed "Add", "Update", and "Cancel" buttons in Customer window
-	Completed input validation within VS in Customer window

2. To do until next Scrum:

-	When unclick customer: CustomerID should disappear. Update button should be Add, Cancel button should work
-	Fixing Customer database total price (add new entity)
-       Working on saleorder and modifying customer windows
-	On-hand quantity and reserved quantity in inventory (calculated field)

3. Need assistance / figure things out
-	Question from teacher: How to fix database connection error


2021/04/13 Mathew
1. Done / not done since last scrum:	
-	Worked on Product window
-	studied database-first tutorial 
-	Fixed Trello user stories

2. To do until next Scrum:
-	Continue working on VS coding windows of Product and Inventory
-	On-hand quantity and reserved quantity in inventory (calculated field) TBD


3. Need assistance / figure things out
-	TBD


2021/04/13 Khaled
1. Done / not done since last scrum:
-	Finalized vendor page with update and delete 
-	Centered every window to owner
2. To do until next Scrum:
-	Fix Final design of vendor interface
-	Start working on purchase order window

3. Need assistance / figure things out
-	TBD

===================================

2021/04/14 Khaled
1. Done / not done since last scrum:
-	Redesigned Purchase_Order XAML
-	Added PlaceOrderPurchase XAML 
-	Fixed Vendor page 

2. To do until next Scrum:
-	Start programming PlaceOrderPurchase 

3. Need assistance / figure things out
-	Get access to enum from database and bind them to wpf


2021/04/14 Ali

1. Done / not done since last scrum:
-	Fixed: When unclick customer: CustomerID should disappear. Update button should be Add, Cancel button should work
-	Redesigned Sale_Order window
-	Added functionality of Add & Delete in Sale_Order window

2. To do until next Scrum:

-	Fixing Order database total price (add new entity)
-       Working on saleorder (Update button), lazy loading
-	On-hand quantity and reserved quantity in inventory (calculated field)
-	LINQ or Lambda to filter customer/vendor product (Lazy loading)
-	Unit-testing study

3. Need assistance / figure things out
-	NA

2021/04/14 Mathew
1. Done / not done since last scrum:	
-	Product window complete, needs more testing
-	Inventory window 80% complete

2. To do until next Scrum:
-	Continue working Inventory window
-	Add missing Warehouse table? Use as text field in Inventory for now.
-	On-hand quantity and reserved quantity in inventory (calculated field) TBD


3. Need assistance / figure things out
-	Program is slow, why? Do 4-8 images cause such a delay?

===================================

2021/04/15 Ali

1. Done / not done since last scrum:

-	Added functionality of "Update" in Sale_Order window

2. To do until next Scrum:

-	Fixing Order database total price (add new entity)
-       Working on saleorder (Update button), lazy loading
-	On-hand quantity and reserved quantity in inventory (calculated field)
-	LINQ or Lambda to filter customer/vendor product (Lazy loading)
-	Unit-testing study

3. Need assistance / figure things out
-	NA


2021/04/15 Khaled
1. Done / not done since last scrum:
-	Finished add and delete functions of purchase table

2. To do until next Scrum:
-	Fix the update function
-	fix enums

3. Need assistance / figure things out
-	How to fix "Total cost" glitch

2021/04/15 Mathew
1. Done / not done since last scrum:	
-	Product window complete, needs more testing
-	Inventory window 95% complete, problem with listview binding to database crashing program

2. To do until next Scrum:
-	Work on receiving PO, Shipping SO functionality.
-	Continue working Inventory window
-	Remove references to Warehouse table.
-	On-hand quantity and reserved quantity in inventory (calculated field) TBD


3. Need assistance / figure things out
-	Problem with listview binding to database crashing program

================================================================================

2021/04/16 Ali

1. Done / not done since last scrum:

-	Did Unit Testing mostly

2. To do until next Scrum:

-	Complete unit testing
-	Fixing Order database total price (add new entity like warehouse id, maybe delete order status if no need to update order)
-       Working on saleorder (Update button) logic 
-	On-hand quantity and reserved quantity in inventory (calculated field)
-	LINQ or Lambda to filter customer/vendor product (Lazy loading)

3. Need assistance / figure things out
-	NA

2021/04/16 Mathew

1. Done / not done since last scrum:	
-	Inventory window complete
-	Watched Wizard tutorial, started on Product creation wix\zard

2. To do until next Scrum:
-	Finish product creation wizard
-	Work on receiving PO, Shipping SO functionality.
-	Remove references to Warehouse table.
-	On-hand quantity and reserved quantity in inventory (calculated field) TBD


3. Need assistance / figure things out
-	NA


2021/04/16 Khaled
1. Done / not done since last scrum:
-	Updated Purchase table
-	Started tutorial on reports

2. To do until next Scrum:
-	Start building reports

3. Need assistance / figure things out
-	How to fix "Total cost" glitch again (not done yet)

============================================================================================

2021/04/19 Mathew

1. Done / not done since last scrum:
-	Product creation Wizard complete

2. To do until next Scrum:
-	Product creation wizard should validate data on each page, not at the end.
-	Fix image error message when updating products
-	Work on receiving PO, Shipping SO functionality.
-	Remove references to Warehouse table.
-	On-hand quantity and reserved quantity in inventory (calculated field) TBD


3. Need assistance / figure things out
-	How to add an event handler on the wizard finish button
-	Fix image error message when updating products


2021/04/19 Ali

1. Done / not done since last scrum:

-	Finished Unit Testing 
-	Added Save to CSV for all windows
-	Added Tab control in the Main window and updated design

2. To do until next Scrum:

-	Testing program

3. Need assistance / figure things out
-	NA

2021/04/19 Khaled
1. Done / not done since last scrum:
-	Read about Installer
-	Got an installer working
-	started buidling report pages (design)

2. To do until next Scrum:
-	Building a connection to reports

3. Need assistance / figure things out
-	How to connect data source to database for reports
-	Understand why installer's .MSI and .EXE not uploading on git