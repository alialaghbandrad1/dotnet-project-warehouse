﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseMS.crystalReports
{
    /// <summary>
    /// Interaction logic for VendorsWindowReport.xaml
    /// </summary>
    public partial class VendorsWindowReport : Window
    {
        public VendorsWindowReport()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
            try
            {
                DataSetsFolder.VendorsDataSet newSet = new DataSetsFolder.VendorsDataSet();
                DataSetsFolder.VendorsDataSetTableAdapters.VendorTableAdapter da = new DataSetsFolder.VendorsDataSetTableAdapters.VendorTableAdapter();
                da.Fill(newSet.Vendor);
                crystalReports.VendorReport report = new crystalReports.VendorReport();
                report.SetDataSource(newSet);
                crViewer.ViewerCore.ReportSource = report;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed: Could not generate vendor data", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }
    }
}
