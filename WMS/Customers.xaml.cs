﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseMS
{
    /// <summary>
    /// Interaction logic for Customers.xaml
    /// </summary>
    public partial class Customers : Window
    {
        warehouseEntities ctx;
        // List<Customer> customerList = new List<Customer>();
        public Customers()
        {
            try
            {
                InitializeComponent();
                ctx = new warehouseEntities();
                lvCustomers.ItemsSource = (from p in ctx.Customers select p).ToList<Customer>();
            }
            catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Fatal error: Database connection failed:\n" + ex.Message);
                Environment.Exit(1); // fatal error
            }

        }

        public Customer currSelCustomer; // null if adding, non-null if editing

        private void btAddUpdateCustomer_Click(object sender, RoutedEventArgs e)
        {
            currSelCustomer = lvCustomers.SelectedItem as Customer;
            if (!inputsValidation()) return;
            if (currSelCustomer == null)
            {

                try
                {
                    Customer c = new Customer
                    {
                        CompanyName = tbNameCompany.Text,
                        ContactName = tbContactNameCustomer.Text,
                        Address = tbAddressCustomer.Text,
                        City = tbCityCustomer.Text,
                        Province = tbProvinceCustomer.Text,
                        Country = tbCountryCustomer.Text,
                        Phone = tbPhoneCustomer.Text
                    };
                    ctx.Customers.Add(c);
                    ctx.SaveChanges();
                    lvCustomers.ItemsSource = (from p in ctx.Customers select p).ToList<Customer>();
                    lvCustomers.Items.Refresh();
                    ClearUserInputs();

                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            else
            {
                try
                {
                    currSelCustomer.CompanyName = tbNameCompany.Text;
                    currSelCustomer.ContactName = tbContactNameCustomer.Text;
                    currSelCustomer.Address = tbAddressCustomer.Text;
                    currSelCustomer.City = tbCityCustomer.Text;
                    currSelCustomer.Province = tbProvinceCustomer.Text;
                    currSelCustomer.Country = tbCountryCustomer.Text;
                    currSelCustomer.Phone = tbPhoneCustomer.Text;
                    ctx.SaveChanges();
                    lvCustomers.ItemsSource = (from p in ctx.Customers select p).ToList<Customer>();
                    lvCustomers.Items.Refresh();
                    ClearUserInputs();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        public void ClearUserInputs()
        {
            lblIdCustomer.Content = "...";
            tbNameCompany.Clear();
            tbContactNameCustomer.Clear();
            tbContactNameCustomer.Clear();
            tbAddressCustomer.Clear();
            tbCityCustomer.Clear();
            tbProvinceCustomer.Clear();
            tbCountryCustomer.Clear();
            tbPhoneCustomer.Clear();
            btDeleteCustomer.IsEnabled = false;
            btCancelCustomer.IsEnabled = true;
        }

        private void btCancelCustomer_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void lvCustomers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currSelCustomer = lvCustomers.SelectedItem as Customer;
            if (currSelCustomer == null)
            {
                ClearUserInputs();
                return;
            }
            else
            {
                Customer c = (Customer)lvCustomers.SelectedItem;
                lblIdCustomer.Content = c.CustomersID;
                btDeleteCustomer.IsEnabled = true;
                btCancelCustomer.IsEnabled = true;
                btAddUpdateCustomer.Content = "Update Customer";
                tbNameCompany.Text = c.CompanyName;
                tbContactNameCustomer.Text = c.ContactName;
                tbAddressCustomer.Text = c.Address;
                tbCityCustomer.Text = c.City;
                tbProvinceCustomer.Text = c.Province;
                tbCountryCustomer.Text = c.Country;
                tbPhoneCustomer.Text = c.Phone;
            }

        }

        private void btDeleteCustomer_Click(object sender, RoutedEventArgs e)
        {
            Customer currSelCustomer = (Customer)lvCustomers.SelectedItem;
            if (currSelCustomer == null) { return; } // should never happen
            if (MessageBoxResult.OK != MessageBox.Show("Do you want to delete the customer " + currSelCustomer + "?\n", "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning))
            { return; } // cancelled
            try
            {
                ctx.Customers.Remove(currSelCustomer);
                ctx.SaveChanges();
                lvCustomers.ItemsSource = (from p in ctx.Customers select p).ToList<Customer>();
                lvCustomers.Items.Refresh();
                ClearUserInputs();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }


        public bool inputsValidation()
        {
            List<string> errorsList = new List<string>();
            if (!Globals.Validator.IsValidCompanyName(tbNameCompany.Text))
            {
                errorsList.Add("Company name must be between 1 and 45 characters");
            }
            if (!Globals.Validator.IsValidContactName(tbContactNameCustomer.Text))
            {
                errorsList.Add("Contact name must be between 1 and 45 characters");
            }
            if (!Globals.Validator.IsValidCustomerPhone(tbPhoneCustomer.Text))
            {
                errorsList.Add("Phone format error. i.e 555-555-5555");
            }
            if (!Globals.Validator.IsValidCustomerAddress(tbAddressCustomer.Text))
            {
                errorsList.Add("Address must be between 1 and 100 characters");
            }
            if (!Globals.Validator.IsValidCustomerCity(tbCityCustomer.Text))
            {
                errorsList.Add("City must be between 1 and 45 characters");
            }
            if (!Globals.Validator.IsValidCustomerProvince(tbProvinceCustomer.Text))
            {
                errorsList.Add("Province\\State must be between 1 and 45 characters");
            }
            if (!Globals.Validator.IsValidCustomerCountry(tbCountryCustomer.Text))
            {
                errorsList.Add("Country must be between 1 and 45 characters");
            }

            if (errorsList.Count > 0)
            {
                MessageBox.Show(string.Join("\n", errorsList), "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        private void btSaveSelected_Click(object sender, RoutedEventArgs e)
        {
            var selItems = lvCustomers.SelectedItems.Cast<Customer>().ToList();
            if (selItems.Count == 0)
            {
                MessageBox.Show(this, "Please select some items first", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            saveFileDialog.FilterIndex = 1;

            if (saveFileDialog.ShowDialog() == true)
            {
                string fileName = saveFileDialog.FileName;
                SaveDataToFile(fileName, selItems);
            }
        }

        private void SaveDataToFile(string fileName, List<Customer> tripsToSave)
        {
            try
            {
                List<string> linesList = new List<string>();
                foreach (Customer t in tripsToSave)
                {
                    linesList.Add(t.ToDataString());
                }
                File.WriteAllLines(fileName, linesList);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show(this, ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }


    }
}
