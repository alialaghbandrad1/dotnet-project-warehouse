﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseMS
{
    /// <summary>
    /// Interaction logic for Purchases.xaml
    /// </summary>
    public partial class Purchases : Window
    {
        warehouseEntities ctx;
        public Purchases()
        {
            try
            {
                InitializeComponent();
                ctx = new warehouseEntities();
                lvPurchaseOrder.ItemsSource = (from p in ctx.Purchase_Order select p).ToList<Purchase_Order>();
                Utils.AutoResizeColumns(lvPurchaseOrder);
                lvPurchaseOrder.Items.Refresh();
            }
            catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Fatal error: Database connection failed:\n" + ex.Message);
                Environment.Exit(1); // fatal error
            }
        }

        private void btCancelPurchase_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        public Vendor currSelectedVendor; // null if adding, non-null if editing
        public Product currSelectedProduct; // null if adding, non-null if editing

        private void btAddPurchase_Click(object sender, RoutedEventArgs e)
        {
            PlacePurchaseOrder dlg = new PlacePurchaseOrder();
            dlg.Owner = this;
            if (dlg.ShowDialog() == true)
            {


                try
                {
                    if (!int.TryParse(dlg.tbQuantityPurchase.Text, out int purchaseQuantity))
                    {
                        MessageBox.Show("Quantity must be an integer");
                        return;
                    }

                    DateTime orderDate = dlg.dpOrderDatePurchase.SelectedDate.Value;
                    // int purchaseQuantity = int.Parse(dlg.tbQuantityPurchase.Text);
                    decimal costPerUnit = dlg.currSelProduct.CostPrice;
                    decimal totalCost = purchaseQuantity * costPerUnit;
                    DateTime shippingDate = dlg.dpShipDatePurchase.SelectedDate.Value;
                    string method = dlg.cbShipMethodPurchase.Text;
                    int idVendor = dlg.currSelVendor.VendorID;
                    int idProduct = dlg.currSelProduct.ProductID;
                    string description = dlg.currSelProduct.Description;
                    string unitOfMesure = dlg.currSelProduct.UnitOfMeasure;

                    Purchase_Order newOrder = new Purchase_Order
                    {
                        ProductID = idProduct,
                        VendorID = idVendor,
                        Description = description,
                        PurchaseQuantity = purchaseQuantity,
                        UnitOfMeasure = unitOfMesure,
                        CostPrice = totalCost,
                        ShipMethod = method,
                        PurchaseDate = orderDate,
                        ShipDate = shippingDate,
                        OrderStatus = dlg.cbStatusSales.Text,
                    };
                    ctx.Purchase_Order.Add(newOrder);
                    ctx.SaveChanges();
                    lvPurchaseOrder.ItemsSource = (from t in ctx.Purchase_Order select t).ToList<Purchase_Order>();
                    Utils.AutoResizeColumns(lvPurchaseOrder);
                    lvPurchaseOrder.Items.Refresh();
                }
                catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
                {
                    Console.WriteLine(ex.StackTrace);
                    MessageBox.Show("Database operation failed:\n" + ex.Message);
                }
            }
        }

        private void btDeletePurchase_Click(object sender, RoutedEventArgs e)
        {
            Purchase_Order currPurchaseOrder = (Purchase_Order)lvPurchaseOrder.SelectedItem;
            if (currPurchaseOrder == null) { return; } // should never happen
            if (MessageBoxResult.OK != MessageBox.Show("Do you want to delete the order?\n " + currPurchaseOrder, "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning))
            { return; } // cancelled
            try
            {
                ctx.Purchase_Order.Remove(currPurchaseOrder);
                ctx.SaveChanges();
                lvPurchaseOrder.ItemsSource = (from p in ctx.Purchase_Order select p).ToList<Purchase_Order>();
                lvPurchaseOrder.Items.Refresh();
                btAddPurchase.IsEnabled = true;
                btDeletePurchase.IsEnabled = false;
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        private void lvPurchaseOrder_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Purchase_Order currPurchaseOrder = (Purchase_Order)lvPurchaseOrder.SelectedItem;

            if (currPurchaseOrder == null)
            {
                return;
            }
            else
            {
                btAddPurchase.IsEnabled = false;
                btDeletePurchase.IsEnabled = true;
            }
        }

        private void btSaveSelected_Click(object sender, RoutedEventArgs e)
        {
            var selItems = lvPurchaseOrder.SelectedItems.Cast<Purchase_Order>().ToList();
            if (selItems.Count == 0)
            {
                MessageBox.Show(this, "Please select some items first", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            saveFileDialog.FilterIndex = 1;
            // saveFileDialog1.RestoreDirectory = true;
            if (saveFileDialog.ShowDialog() == true)
            {
                string fileName = saveFileDialog.FileName;
                SaveDataToFile(fileName, selItems);
            }
        }

        private void SaveDataToFile(string fileName, List<Purchase_Order> tripsToSave)
        {
            try
            {
                List<string> linesList = new List<string>();
                foreach (Purchase_Order t in tripsToSave)
                {
                    linesList.Add(t.ToDataString());
                }
                File.WriteAllLines(fileName, linesList);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show(this, ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

    }
}
