﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseMS
{
    /// <summary>
    /// Interaction logic for ReportsWindow.xaml
    /// </summary>
    public partial class ReportsWindow : Window
    {
        public ReportsWindow()
        {
            InitializeComponent();
        }

        private void tbVendorReport_MouseEnter(object sender, MouseEventArgs e)
        {
            tbVendorReport.Foreground = Brushes.Blue;
            tbVendorReport.TextDecorations = TextDecorations.Underline;
        }

        private void tbVendorReport_MouseLeave(object sender, MouseEventArgs e)
        {
            tbVendorReport.Foreground = Brushes.Black;
            tbVendorReport.TextDecorations = null;
        }

        private void tbVendorReport_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            crystalReports.VendorsWindowReport dlg = new crystalReports.VendorsWindowReport();
            dlg.Owner = this;
            dlg.ShowDialog();
        }
    }
}
