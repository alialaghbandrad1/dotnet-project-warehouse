using Microsoft.VisualStudio.TestTools.UnitTesting;
using WarehouseMS;

namespace TestWarehouseMS
{
    [TestClass]
    public class WarehouseTests
    {
        [TestMethod]
        public void Validate_inputValidation_CustomerCompanyName()
        {
            // Arrange
            var cust = new AllValidations();
            string name = "TradeCo7";
            // Act
            cust.IsValidCompanyName(name);
            // Assert
            Assert.IsFalse(cust.IsValidCompanyName(name));
        }

        [TestMethod]
        public void Validate_inputValidation_CustomerContactName()
        {
            // Arrange
            var cust = new AllValidations();
            string name = "Philip20";
            // Act
            cust.IsValidCompanyName(name);
            // Assert
            Assert.IsFalse(cust.IsValidContactName(name));
        }

        [TestMethod]
        public void Validate_inputValidation_CustomerPhoneNumber()
        {
            // Arrange
            var cust = new AllValidations();
            string phone = "5143332299";
            // Act
            cust.IsValidCustomerPhone(phone);
            // Assert
            Assert.IsFalse(cust.IsValidCustomerPhone(phone));
        }

        [TestMethod]
        public void Validate_inputValidation_CustomerAddress()
        {
            // Arrange
            var cust = new AllValidations();
            var address = "@47 Ave Kings";
            // Act
            cust.IsValidCustomerAddress(address);
            // Assert
            Assert.IsFalse(cust.IsValidCustomerAddress(address));
        }

        [TestMethod]
        public void Validate_inputValidation_CustomerCity()
        {
            // Arrange
            var cust = new AllValidations();
            string city = "Montreal8";
            // Act
            cust.IsValidCustomerCity(city);
            // Assert
            Assert.IsFalse(cust.IsValidCustomerCity(city));
        }

        [TestMethod]
        public void Validate_inputValidation_CustomerProvince()
        {
            // Arrange
            var cust = new AllValidations();
            string province = "@Quebec";
            // Act
            cust.IsValidCustomerProvince(province);
            // Assert
            Assert.IsFalse(cust.IsValidCustomerProvince(province));
        }

        [TestMethod]
        public void Validate_inputValidation_Country()
        {
            // Arrange
            var cust = new AllValidations();
            string country = "Canada+";
            // Act
            cust.IsValidCustomerCountry(country);
            // Assert
            Assert.IsFalse(cust.IsValidCustomerCountry(country));
        }

        [TestMethod]
        public void Validate_inputValidation_SelectCustomer()
        {
            // Arrange
            var saleOrder = new AllValidations();
            Customer customer = null;
            // Act
            saleOrder.IsCustomerSelected(customer);
            // Assert
            Assert.IsFalse(saleOrder.IsCustomerSelected(customer));
        }

        [TestMethod]
        public void Validate_inputValidation_SelectVendor()
        {
            // Arrange
            var purchaseOrder = new AllValidations();
            Vendor vendor = null;
            // Act
            purchaseOrder.IsVendorSelected(vendor);
            // Assert
            Assert.IsFalse(purchaseOrder.IsVendorSelected(vendor));
        }

        [TestMethod]
        public void Validate_inputValidation_SelectProduct()
        {
            // Arrange
            var saleOrder = new AllValidations();
            Product product = null;
            // Act
            saleOrder.IsProductSelected(product);
            // Assert
            Assert.IsFalse(saleOrder.IsProductSelected(product));
        }

        [TestMethod]
        public void Validate_inputValidation_SelectShipDate()
        {
            // Arrange
            var saleOrder = new AllValidations();
            string shipDate = "";
            // Act
            saleOrder.IsShipDateSelected(shipDate);
            // Assert
            Assert.IsFalse(saleOrder.IsShipDateSelected(shipDate));
        }

        [TestMethod]
        public void Validate_inputValidation_IntegerQuantity()
        {
            // Arrange
            var saleOrder = new AllValidations();
            string quantity = "Q";
            // Act
            saleOrder.IsSQuantityInteger(quantity);
            // Assert
            Assert.IsFalse(saleOrder.IsSQuantityInteger(quantity));
        }

    }
}
