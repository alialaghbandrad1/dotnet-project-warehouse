﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseMS
{
    /// <summary>
    /// Interaction logic for PlacePurchaseOrder.xaml
    /// </summary>
    public partial class PlacePurchaseOrder : Window
    {
        warehouseEntities ctx;
        public PlacePurchaseOrder()
        {
            try
            {
                InitializeComponent();
                ctx = new warehouseEntities();
                lvProducts.ItemsSource = (from p in ctx.Products select p).ToList<Product>();
                lvVendors.ItemsSource = (from t in ctx.Vendors select t).ToList<Vendor>();
                dpOrderDatePurchase.SelectedDate = DateTime.Today;
                dpShipDatePurchase.SelectedDate = DateTime.Today;
            }
            catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Fatal error: Database connection failed:\n" + ex.Message);
                Environment.Exit(1); // fatal error
            }
        }

        public Vendor currSelVendor;
        public Product currSelProduct;

        private void btPlaceOrderPurchase_Click(object sender, RoutedEventArgs e)
        {
            if (!inputsValidation()) return;

            if (!int.TryParse(tbQuantityPurchase.Text, out int purchaseQuantity))
            {
                MessageBox.Show("Quantity must be an integer");
                return;
            }

            currSelVendor = (Vendor)lvVendors.SelectedItem;
            currSelProduct = (Product)lvProducts.SelectedItem;

            decimal costPerUnit = currSelProduct.CostPrice;
            decimal totalCost = purchaseQuantity * costPerUnit;

            DialogResult = true; // assign result and dissmiss dialog

        }

        public bool inputsValidation()
        {
            Vendor currSelVendor = (Vendor)lvVendors.SelectedItem;
            Product currSelProduct = (Product)lvProducts.SelectedItem;
            List<string> errorsList = new List<string>();
            if (!Globals.Validator.IsVendorSelected(currSelVendor))
            {
                errorsList.Add("Vendor must be selected");
            }
            if (!Globals.Validator.IsProductSelected(currSelProduct))
            {
                errorsList.Add("Product must be selected");
            }

            if (!Globals.Validator.IsSQuantityInteger(tbQuantityPurchase.Text))
            {
                errorsList.Add("Quantity must be integer");
            }

            if (!Globals.Validator.IsShipDateSelected(dpShipDatePurchase.SelectedDate.ToString()))
            {
                errorsList.Add("Ship date must be selected");
            }

            if (errorsList.Count > 0)
            {
                MessageBox.Show(string.Join("\n", errorsList), "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        private void lvVendors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currSelVendor = (Vendor)lvVendors.SelectedItem;
            if (currSelVendor == null)
            {
                return;
            }
            else
            {
                lvProducts.ItemsSource = (from p in ctx.Products select p).Where(s => s.VendorID == currSelVendor.VendorID).ToList<Product>();
            }
        }

        private void lvProducts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currSelProduct = (Product)lvProducts.SelectedItem;
            if (currSelProduct == null)
            {
                lblQuanityUnitPurchase.Content = "-";
            }
            else
            {
                lblQuanityUnitPurchase.Content = currSelProduct.UnitOfMeasure;
                if (!int.TryParse(tbQuantityPurchase.Text, out int purchaseQuantity))
                {
                    MessageBox.Show("Please enter the quantity of product");
                    return;
                }

                decimal costPerUnit = currSelProduct.CostPrice;
                decimal totalCost = purchaseQuantity * costPerUnit;
                // lblTotalCostPurchase.Content = decimal.Parse(String.Format("{0:0.00}", totalCost));
                decimal costPrice = decimal.Parse(string.Format("{0:0.00}", lblTotalCostPurchase.Content));
 
            }
        }

        private void btCancelPurchase_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


        private void tbQuantityPurchase_TextChanged(object sender, TextChangedEventArgs e)
        {
            currSelProduct = (Product)lvProducts.SelectedItem;

            if (currSelProduct == null)
            {
                lblTotalCostPurchase.Content = "...";
                MessageBox.Show("Product must be selected");
                return;
            }
            if (!int.TryParse(tbQuantityPurchase.Text, out int purchaseQuantity))
            {
                MessageBox.Show("Quantity must be an integer");
                return;
            }

            decimal costPerUnit = currSelProduct.CostPrice;
            decimal totalCost = purchaseQuantity * costPerUnit;
            lblTotalCostPurchase.Content = totalCost;
        }
    }
}
