﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseMS
{
    /// <summary>
    /// Interaction logic for Vendors.xaml
    /// </summary>
    public partial class Vendors : Window
    {
        warehouseEntities ctx;
        public Vendors()
        {
            try
            {
                InitializeComponent();
                ctx = new warehouseEntities();
                lvVendors.ItemsSource = (from t in ctx.Vendors select t).ToList<Vendor>();
                Utils.AutoResizeColumns(lvVendors);
            }
            catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Fatal error: Database connection failed:\n" + ex.Message);
                Environment.Exit(1); // fatal error
            }
        }

        public Vendor currSelVendor;
        private void btAddUpdateVendor_Click(object sender, RoutedEventArgs e)
        {
            if (!inputValidation()) return;
            currSelVendor = lvVendors.SelectedItem as Vendor;
            if (currSelVendor == null)
            {
                try
                {
                    string name = tbNameVendor.Text;
                    string contact = tbContactNameVendor.Text;
                    string phone = tbPhoneVendor.Text;
                    string address = tbAddressVendor.Text;
                    string city = tbCityVendor.Text;
                    string province = tbProvinceVendor.Text;
                    string country = tbCountryVendor.Text;
                    Vendor newVendor = new Vendor
                    {
                        VendorName = name,
                        ContactName = contact,
                        Address = address,
                        City = city,
                        Province = province,
                        Country = country,
                        Phone = phone
                    };
                    ctx.Vendors.Add(newVendor);
                    ctx.SaveChanges();
                    lvVendors.ItemsSource = (from t in ctx.Vendors select t).ToList<Vendor>();
                    Utils.AutoResizeColumns(lvVendors);
                    lvVendors.Items.Refresh();
                    ClearUserInputs();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }

            }
            else
            {
                try
                {
                    currSelVendor.VendorName = tbNameVendor.Text;
                    currSelVendor.ContactName = tbContactNameVendor.Text;
                    currSelVendor.Phone = tbPhoneVendor.Text;
                    currSelVendor.Address = tbAddressVendor.Text;
                    currSelVendor.City = tbCityVendor.Text;
                    currSelVendor.Province = tbProvinceVendor.Text;
                    currSelVendor.Country = tbCountryVendor.Text;
                    ctx.SaveChanges();
                    lvVendors.ItemsSource = (from t in ctx.Vendors select t).ToList<Vendor>();
                    Utils.AutoResizeColumns(lvVendors);
                    lvVendors.Items.Refresh();
                    ClearUserInputs();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        public void ClearUserInputs()
        {
            lblIdVendor.Content = "...";
            tbNameVendor.Clear();
            tbContactNameVendor.Clear();
            tbPhoneVendor.Clear();
            tbAddressVendor.Clear();
            tbCityVendor.Clear();
            tbProvinceVendor.Clear();
            tbCountryVendor.Clear();
            btDeleteVendor.IsEnabled = false;
        }

        bool inputValidation()
        {
            List<string> errorsList = new List<string>();
            string name = tbNameVendor.Text;
            string accents = "àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸ";
            if (!Regex.IsMatch(name, @"^[a-zA-Z" + accents +" '-]{1,45}$"))
            {
                errorsList.Add("Name must be between 1 and 50 characters");
            }
            string contact = tbContactNameVendor.Text;
            if (!Regex.IsMatch(contact, @"^[a-zA-Z" + accents + " '-]{1,45}$"))
            {
                errorsList.Add("Contact Name must be between 1 and 50 characters");
            }
            string phone = tbPhoneVendor.Text;
            if (!Regex.IsMatch(phone, @"^[1-9]\d{2}-\d{3}-\d{4}$"))
            {
                errorsList.Add("Phone format error. i.e 555-555-5555");
            }
            string address = tbAddressVendor.Text;
            if (!Regex.IsMatch(address, @"^[a-zA-Z\d" + accents + " #&'-]{1,100}$"))
            {
                errorsList.Add("Address must be between 1 and 60 characters");
            }
            string city = tbCityVendor.Text;
            if (!Regex.IsMatch(city, @"^[a-zA-Z" + accents + " '-]{1,45}$"))
            {
                errorsList.Add("City must be between 1 and 50 characters");
            }
            string province = tbProvinceVendor.Text;
            if (!Regex.IsMatch(province, @"^[a-zA-Z" + accents + " '-]{1,45}$"))
            {
                errorsList.Add("Province\\State must be between 1 and 30 characters");
            }
            string country = tbCountryVendor.Text;
            if (!Regex.IsMatch(country, @"^[a-zA-Z" + accents + " '-]{1,45}$"))
            {
                errorsList.Add("Country must be between 1 and 50 characters");
            }
            if (errorsList.Count > 0)
            {
                MessageBox.Show(string.Join("\n", errorsList), "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        private void btCancelVendor_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void lvVendors_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currSelVendor = lvVendors.SelectedItem as Vendor;
            if (currSelVendor == null)
            {
                btAddUpdateVendor.Content = "Add/Update";
                ClearUserInputs();
                return;
            }
            else
            {
                Vendor updatedVendor = (Vendor)lvVendors.SelectedItem;
                lblIdVendor.Content = updatedVendor.VendorID;
                tbNameVendor.Text = updatedVendor.VendorName;
                tbContactNameVendor.Text = updatedVendor.ContactName;
                tbAddressVendor.Text = updatedVendor.Address;
                tbCityVendor.Text = updatedVendor.City;
                tbProvinceVendor.Text = updatedVendor.Province;
                tbCountryVendor.Text = updatedVendor.Country;
                tbPhoneVendor.Text = updatedVendor.Phone;
                btDeleteVendor.IsEnabled = true;
                btAddUpdateVendor.Content = "Update";
            }
        }

        private void btDeleteVendor_Click(object sender, RoutedEventArgs e)
        {
            Vendor currSelVendor = (Vendor)lvVendors.SelectedItem;
            if (currSelVendor == null) { return; } // should never happen
            if (MessageBoxResult.OK != MessageBox.Show("Do you want to delete the Vendor?" + currSelVendor + "?\n", "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning))
            { return; } // cancelled
            try
            {
                ctx.Vendors.Remove(currSelVendor);
                ctx.SaveChanges();
                lvVendors.ItemsSource = (from t in ctx.Vendors select t).ToList<Vendor>();
                Utils.AutoResizeColumns(lvVendors);
                lvVendors.Items.Refresh();
                ClearUserInputs();
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database deletion operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }

        }

        private void btSaveSelected_Click(object sender, RoutedEventArgs e)
        {
            var selItems = lvVendors.SelectedItems.Cast<Vendor>().ToList();
            if (selItems.Count == 0)
            {
                MessageBox.Show(this, "Please select some items first", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            saveFileDialog.FilterIndex = 1;

            if (saveFileDialog.ShowDialog() == true)
            {
                string fileName = saveFileDialog.FileName;
                SaveDataToFile(fileName, selItems);
            }
        }

        private void SaveDataToFile(string fileName, List<Vendor> tripsToSave)
        {
            try
            {
                List<string> linesList = new List<string>();
                foreach (Vendor t in tripsToSave)
                {
                    linesList.Add(t.ToDataString());
                }
                File.WriteAllLines(fileName, linesList);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show(this, ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

    }
}
