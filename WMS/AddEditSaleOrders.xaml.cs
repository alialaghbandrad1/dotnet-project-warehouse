﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseMS
{
    /// <summary>
    /// Interaction logic for AddEditSaleOrders.xaml
    /// </summary>
    public partial class AddEditSaleOrders : Window
    {
        warehouseEntities ctx;

        public AddEditSaleOrders()
        {
            try
            {
                InitializeComponent();
                ctx = new warehouseEntities();
                lvCustomers.ItemsSource = (from p in ctx.Customers select p).ToList<Customer>();
                lvProducts.ItemsSource = (from p in ctx.Products select p).ToList<Product>();

            }
            catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Fatal error: Database connection failed:\n" + ex.Message);
                Environment.Exit(1); // fatal error
            }
        }

        public Customer currSelectedCustomer; // null if adding, non-null if editing
        public Product currSelectedProduct; // null if adding, non-null if editing


        private void AddSaleOrder_Click(object sender, RoutedEventArgs e)
        {
            if (!inputsValidation()) return;

            if (!int.TryParse(tbQuantitySales.Text, out int SaleQuantity))
            {
                MessageBox.Show("Quantity must be an integer");
                return;
            }

            decimal costPerUnit = currSelectedProduct.SellingPrice;
            decimal totalPrice = SaleQuantity * costPerUnit;

            DialogResult = true; // assign result and dissmiss dialog
        }

        public bool inputsValidation()
        {
            Customer currSelCustomer = (Customer)lvCustomers.SelectedItem;
            Product currSelProduct = (Product)lvProducts.SelectedItem;
            List<string> errorsList = new List<string>();
            if (!Globals.Validator.IsCustomerSelected(currSelCustomer))
            {
                errorsList.Add("Customer must be selected");
            }
            if (!Globals.Validator.IsProductSelected(currSelProduct))
            {
                errorsList.Add("Product must be selected");
            }

            if (!Globals.Validator.IsSQuantityInteger(tbQuantitySales.Text))
            {
                errorsList.Add("Quantity must be integer");
            }

            if (!Globals.Validator.IsShipDateSelected(dpShipDateSales.SelectedDate.ToString()))
            {
                errorsList.Add("Ship date must be selected");
            }

            if (errorsList.Count > 0)
            {
                MessageBox.Show(string.Join("\n", errorsList), "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        private void btCancelSales_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void lvProducts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currSelectedProduct = (Product)lvProducts.SelectedItem;
            if (currSelectedProduct == null)
            {
                lblQuanityUnitPurchase.Content = "-";
            }
            else
            {
                lblQuanityUnitPurchase.Content = currSelectedProduct.UnitOfMeasure;

                if (!int.TryParse(tbQuantitySales.Text, out int SaleQuantity))
                {
                    MessageBox.Show("Please enter the quantity of product");
                    return;
                }

                int purchaseQuantity = SaleQuantity;
                decimal costPerUnit = currSelectedProduct.SellingPrice;
                decimal totalCost = purchaseQuantity * costPerUnit;
                lblTotalSalesPrice.Content = totalCost;

            }
        }

        private void tbQuantityPurchase_TextChanged(object sender, TextChangedEventArgs e)
        {
            currSelectedProduct = (Product)lvProducts.SelectedItem;


            if (currSelectedProduct == null)
            {
                lblTotalSalesPrice.Content = "...";
                MessageBox.Show("Product must be selected");
                return;
            }


            if (!int.TryParse(tbQuantitySales.Text, out int SaleQuantity))
            {
                MessageBox.Show("Quantity must be an integer");
                return;
            }
            int purchaseQuantity = SaleQuantity;
            decimal costPerUnit = currSelectedProduct.SellingPrice;
            decimal totalCost = purchaseQuantity * costPerUnit;
            lblTotalSalesPrice.Content = totalCost;

        }

    }
}
