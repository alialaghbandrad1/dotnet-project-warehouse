﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseMS
{
    /// <summary>
    /// Interaction logic for Products.xaml
    /// </summary>
    public partial class Products : Window
    {
        warehouseEntities ctx;


        public decimal SellingProductPrice;
        public decimal CostProductPrice;

        public Products()
        {
            try
            {
                InitializeComponent();
                ctx = new warehouseEntities(); // ex SystemException
                lvProducts.ItemsSource = (from p in ctx.Products select p).ToList<Product>();
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error, could not connect to the database.
            }
        }

        public Product currSelProduct; // null if adding, non-null if editing
        private void btAddUpdateProduct_Click(object sender, RoutedEventArgs e)
        {  
            currSelProduct = lvProducts.SelectedItem as Product;
            if (!AreInputsValid()) return;
            if (currSelProduct == null)
            {
                try
                {
                    string Description = tbDescriptionProduct.Text;
                    string UnitOfMeasure = cbUnitOfMesureProduct.Text;
                    decimal SellingPrice = decimal.Parse(String.Format("{0:0.00}", tbSellingPriceProduct.Text)); // decimal.Parse(tbSellingPriceProduct.Text);
                    decimal CostPrice = decimal.Parse(String.Format("{0:0.00}", tbCostPriceProduct.Text));
                    int VendorID = int.Parse(tbVendorIdProduct.Text);
                    BitmapImage bitmap = Utils.ByteArrayToBitmapImage(currProductImage); // ex: SystemException

                    Product products = new Product
                    {
                        Description = Description,
                        UnitOfMeasure = UnitOfMeasure, // FIXME?
                        SellingPrice = SellingPrice,
                        CostPrice = CostPrice,
                        VendorID = VendorID,
                        ProductImage = currProductImage,
                    };
                    ctx.Products.Add(products); // ex SystemException
                    ctx.SaveChanges(); // ex SystemException
                    lvProducts.ItemsSource = (from p in ctx.Products select p).ToList<Product>();
                    Utils.AutoResizeColumns(lvProducts);
                    lvProducts.Items.Refresh();
                    ClearInputs();
                    //ReloadRecords();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            
            else
            {
                try
                {
                    currSelProduct.Description = tbDescriptionProduct.Text;
                    currSelProduct.UnitOfMeasure = cbUnitOfMesureProduct.Text;
                    currSelProduct.SellingPrice = decimal.Parse(String.Format("{0:0.00}", tbSellingPriceProduct.Text));
                    currSelProduct.CostPrice = decimal.Parse(String.Format("{0:0.00}", tbCostPriceProduct.Text));
                    currSelProduct.VendorID = int.Parse(tbVendorIdProduct.Text);
                    currSelProduct.ProductImage = currProductImage;
                    ctx.SaveChanges();
                    lvProducts.ItemsSource = (from p in ctx.Products select p).ToList<Product>();
                    Utils.AutoResizeColumns(lvProducts);
                    lvProducts.Items.Refresh();
                    ClearInputs();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
        private void btDeleteProduct_Click(object sender, RoutedEventArgs e)
        {
            if (lvProducts.SelectedValue == null)
            {
                MessageBox.Show(this, "Please select an item to delete", "Selection error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            Product selProduct = (Product)lvProducts.SelectedItem;
            MessageBoxResult result = MessageBox.Show("Do you want to delete: " + selProduct.ToString() + " ?", "Data save", MessageBoxButton.YesNo, MessageBoxImage.Warning);
            if (result == MessageBoxResult.Yes)
            {
                try
                {
                    ctx.Products.Remove(selProduct); // ex SystemException
                    ctx.SaveChanges(); // ex SystemException
                    lvProducts.ItemsSource = (from p in ctx.Products select p).ToList<Product>();
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Item cannot be removed if it has inventory", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }


        byte[] currProductImage; // currently selected image, null if none
        private void btImage_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png|All Files (*.*)|*.*";
            //dlg.RestoreDirectory = true;

            if (dlg.ShowDialog() == true)
            {
                try
                {
                    currProductImage = File.ReadAllBytes(dlg.FileName); // ex IOException
                    tbImage.Visibility = Visibility.Hidden; // hide text on the button
                    BitmapImage bitmap = Utils.ByteArrayToBitmapImage(currProductImage); // ex: SystemException
                    imagePreview.Source = bitmap;
                }
                catch (Exception ex) when (ex is SystemException || ex is IOException)
                {
                    Console.WriteLine(ex);
                    MessageBox.Show(this, ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }
        public void ClearInputs()
        {
            tbDescriptionProduct.Text = "";
            cbUnitOfMesureProduct.SelectedItem = "Each";
            tbSellingPriceProduct.Text = "";
            tbCostPriceProduct.Text = "";
            imagePreview.Source = null;
            btDeleteProduct.IsEnabled = false;
            tbImage.Visibility = Visibility.Visible;
        }
        private void lvProducts_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currSelProduct = lvProducts.SelectedItem as Product;
            if (currSelProduct == null)
            {
                btAddUpdateProduct.Content = "Add/Update";
                ClearInputs();
                return;
            }

            else
            {
                try
                {
                    Product updatedProduct = (Product)lvProducts.SelectedItem;
                    tbDescriptionProduct.Text = updatedProduct.Description;
                    cbUnitOfMesureProduct.Text = updatedProduct.UnitOfMeasure;
                    tbSellingPriceProduct.Text = $"{updatedProduct.SellingPrice:###,##0.00}";
                    tbCostPriceProduct.Text = $"{updatedProduct.CostPrice:###,##0.00}";
                    tbVendorIdProduct.Text = updatedProduct.VendorID.ToString();
                    tbImage.Visibility = Visibility.Hidden; // hide text on the button
                    currProductImage = updatedProduct.ProductImage;
                    BitmapImage bitmap = Utils.ByteArrayToBitmapImage(updatedProduct.ProductImage); // ex: SystemException
                    imagePreview.Source = bitmap;
                    btDeleteProduct.IsEnabled = true;
                    btAddUpdateProduct.Content = "Update";
                }
                catch (SystemException ex)
                {
                    Console.WriteLine(ex);
                    MessageBox.Show(ex.Message, "Loading image failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            if (MessageBox.Show("Do you want to leave?", "Confirmation", MessageBoxButton.YesNo, MessageBoxImage.Warning) != MessageBoxResult.No)
            {
                this.Close();
            }
        }

        bool AreInputsValid()
        {
            // Collect the problems, then show them at the end:
            List<string> errorsList = new List<string>();
            if (tbDescriptionProduct.Text.Length < 2 || tbDescriptionProduct.Text.Length > 100)
            {
                errorsList.Add("Description must be between 2 and 100 characters");
            }
            if (cbUnitOfMesureProduct == null) // should never happen
            {
                errorsList.Add("You must select a Unit of Measure");
            }
            if (!Regex.IsMatch(tbSellingPriceProduct.Text, @"^[0-9]{1,6}(\.([0-9]{2})?)$"))
            {
                errorsList.Add("Selling price must be a decimal number with 2 decimal places, e.g. 5.99");
            }
            if (!Regex.IsMatch(tbCostPriceProduct.Text, @"^[0-9]{1,6}(\.([0-9]{2})?)$"))
            {
                errorsList.Add("Cost price must be a decimal number with 2 decimal places, e.g. 5.99");
            }
            if (tbVendorIdProduct == null)
            {
                errorsList.Add("Vendor ID must be a number");
            }
            if (currProductImage == null)
            {
                errorsList.Add("You must choose a picture");
            }
            if (errorsList.Count > 0)
            {
                MessageBox.Show(string.Join("\n", errorsList), "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        private void Wizard_Click(object sender, RoutedEventArgs e)
        {
            var win = new WizardWindow();
            win.ShowDialog();
            if (win.LastPage.CanFinish == true)
            {
                lvProducts.ItemsSource = (from p in ctx.Products select p).ToList<Product>();
                lvProducts.Items.Refresh();
                MessageBox.Show("Success! Your product has been created.");
               
            }
                
        }

        private void btSaveSelected_Click(object sender, RoutedEventArgs e)
        {
            var selItems = lvProducts.SelectedItems.Cast<Product>().ToList();
            if (selItems.Count == 0)
            {
                MessageBox.Show(this, "Please select some items first", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            saveFileDialog.FilterIndex = 1;

            if (saveFileDialog.ShowDialog() == true)
            {
                string fileName = saveFileDialog.FileName;
                SaveDataToFile(fileName, selItems);
            }
        }

        private void SaveDataToFile(string fileName, List<Product> tripsToSave)
        {
            try
            {
                List<string> linesList = new List<string>();
                foreach (Product t in tripsToSave)
                {
                    linesList.Add(t.ToDataString());
                }
                File.WriteAllLines(fileName, linesList);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show(this, ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        
    }
}
