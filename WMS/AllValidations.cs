﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace WarehouseMS
{
    public class AllValidations
    {
        public bool IsValidCompanyName(string companyname)
        {
            string accents = "àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸ";
            if (Regex.IsMatch(companyname, @"^[a-zA-Z" + accents + " '-]{1,45}$"))
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        public bool IsValidContactName(string contactname)
        {
            string accents = "àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸ";
            if (Regex.IsMatch(contactname, @"^[a-zA-Z" + accents + " '-]{1,45}$"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsValidCustomerPhone(string phone)
        {
            if (Regex.IsMatch(phone, @"^[1-9]\d{2}-\d{3}-\d{4}$"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsValidCustomerAddress(string address)
        {
            string accents = "àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸ";
            if (Regex.IsMatch(address, @"^[a-zA-Z\d" + accents + " #&'-]{1,100}$"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsValidCustomerCity(string city)
        {
            string accents = "àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸ";
            if (Regex.IsMatch(city, @"^[a-zA-Z" + accents + " '-]{1,45}$"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsValidCustomerProvince(string province)
        {
            string accents = "àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸ";
            if (Regex.IsMatch(province, @"^[a-zA-Z" + accents + " '-]{1,45}$"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsValidCustomerCountry(string country)
        {
            string accents = "àèìòùÀÈÌÒÙáéíóúýÁÉÍÓÚÝâêîôûÂÊÎÔÛãñõÃÑÕäëïöüÿÄËÏÖÜŸ";
            if (Regex.IsMatch(country, @"^[a-zA-Z" + accents + " '-]{1,45}$"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsCustomerSelected(Customer customer)
        {
            if (customer != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsVendorSelected(Vendor vendor)
        {
            if (vendor != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsProductSelected(Product product)
        {
            if (product != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsSQuantityInteger(string quantity)
        {
            if (Regex.IsMatch(quantity, @"^[0-9]{1,6}$"))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsShipDateSelected(string shipDate)
        {
            if (shipDate != "")
            {
                return true;
            }
            else
            {
                return false;
            }
        }


    }
}
