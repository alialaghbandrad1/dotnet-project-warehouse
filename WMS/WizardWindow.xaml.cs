﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Xceed.Wpf.Toolkit.Core;

namespace WarehouseMS
{
    /// <summary>
    /// Interaction logic for WizardWindow.xaml
    /// </summary>
    public partial class WizardWindow : Window
    {
        warehouseEntities ctx;

        public WizardWindow()
        {
            try
            {
                InitializeComponent();
                ctx = new warehouseEntities(); // ex SystemException
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex);
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error, could not connect to the database.
            }
        }

        private void tbDescriptionProduct_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbDescriptionProductWizard.Text))
            {
                Page1.CanSelectNextPage = false;
            }
            else
            {
                Page1.CanSelectNextPage = true;
            }
        }

        
        private void tbCostPriceProduct_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbSellingPriceProductWizard.Text))
            {
                Page2.CanSelectNextPage = false;
            }
            else
            {
                Page2.CanSelectNextPage = true;
            }
        }

        private void tbSellingPriceProduct_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbCostPriceProductWizard.Text))
            {
                Page2.CanSelectNextPage = false;
            }
            else
            {
                Page2.CanSelectNextPage = true;
            }

        }

        private void tbVendorIdProduct_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (string.IsNullOrEmpty(tbVendorIdProductWizard.Text))
            {
                LastPage.CanFinish = false;
            }
            if (buttonImageWasClicked == false)
            {
                LastPage.CanFinish = false;
            }
            else
            {
                LastPage.CanFinish = true;
            }
        }

        byte[] currProductImageWizard; // currently selected image, null if none

        public bool buttonImageWasClicked = false;
        private void btImage_Click(object sender, RoutedEventArgs e)
        {
            buttonImageWasClicked = true;
            Console.WriteLine(buttonImageWasClicked); // test if button was clicked
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png|All Files (*.*)|*.*";

            if (dlg.ShowDialog() == true)
            {
                try
                {
                    currProductImageWizard = File.ReadAllBytes(dlg.FileName); // ex IOException
                    tbImageWizard.Visibility = Visibility.Hidden; // hide text on the button
                    BitmapImage bitmap = Utils.ByteArrayToBitmapImage(currProductImageWizard); // ex: SystemException
                    imagePreviewWizard.Source = bitmap;
                }
                catch (Exception ex) when (ex is SystemException || ex is IOException)
                {
                    Console.WriteLine(ex);
                    MessageBox.Show(this, ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            }
            if (buttonImageWasClicked == false)
            {
                LastPage.CanFinish = false;
            }
            if (!AreWizardInputsValid()) return;
            else
            {
                LastPage.CanFinish = true;
                addProduct();
            }

        }

        
        bool AreWizardInputsValid()
        {
            // Collect the problems, then show them at the end:
            List<string> errorsList = new List<string>();
            if (tbDescriptionProductWizard.Text.Length < 2 || tbDescriptionProductWizard.Text.Length > 100)
            {
                errorsList.Add("Description must be between 2 and 100 characters");
            }
            if (cbUnitOfMesureProductWizard == null) // should never happen
            {
                errorsList.Add("You must select a Unit of Measure");
            }
            if (!Regex.IsMatch(tbCostPriceProductWizard.Text, @"^[0-9]{1,6}(\.([0-9]{2})?)$"))
            {
                errorsList.Add("Cost price must be a decimal number with 2 decimal places, e.g. 5.99");
            }
            if (!Regex.IsMatch(tbSellingPriceProductWizard.Text, @"^[0-9]{1,6}(\.([0-9]{2})?)$"))
            {
                errorsList.Add("Selling price must be a decimal number with 2 decimal places, e.g. 5.99");
            }
            if (tbVendorIdProductWizard == null)
            {
                errorsList.Add("Vendor ID must be a whole number");
            }
            if (currProductImageWizard == null)
            {
                errorsList.Add("You must choose a picture");
            }
           
            if (errorsList.Count > 0)
            {
                MessageBox.Show(string.Join("\n", errorsList), "Validation error", MessageBoxButton.OK, MessageBoxImage.Warning);
                return false;
            }
            return true;
        }

        private void addProduct()
        {
            try
            {
                string Description = tbDescriptionProductWizard.Text;
                string UnitOfMeasure = cbUnitOfMesureProductWizard.Text;
                decimal SellingPrice = decimal.Parse(String.Format("{0:0.00}", tbSellingPriceProductWizard.Text));
                decimal CostPrice = decimal.Parse(String.Format("{0:0.00}", tbCostPriceProductWizard.Text)); 
                int VendorID = int.Parse(tbVendorIdProductWizard.Text);
                BitmapImage bitmap = Utils.ByteArrayToBitmapImage(currProductImageWizard); // ex: SystemException

                Product products = new Product
                {
                    Description = Description,
                    UnitOfMeasure = UnitOfMeasure,
                    SellingPrice = SellingPrice,
                    CostPrice = CostPrice,
                    VendorID = VendorID,
                    ProductImage = currProductImageWizard,
                };
                ctx.Products.Add(products); // ex SystemException
                ctx.SaveChanges(); // ex SystemException
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }

    }
}


