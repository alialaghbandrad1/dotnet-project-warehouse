﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WarehouseMS
{
    /// <summary>
    /// Interaction logic for Sales.xaml
    /// </summary>
    public partial class Sales : Window
    {
        warehouseEntities ctx;

        public Sales()
        {
            try
            {
                InitializeComponent();
                ctx = new warehouseEntities();
                lvSalesOrder.ItemsSource = (from p in ctx.Sale_Order select p).ToList<Sale_Order>();
                lvSalesOrder.Items.Refresh();
            }
            catch (SystemException ex) // catch-all for EF, SQL and many other exceptions
            {
                Console.WriteLine(ex.StackTrace);
                MessageBox.Show("Fatal error: Database connection failed:\n" + ex.Message);
                Environment.Exit(1); // fatal error
            }
        }

        public Customer currSelectedCustomer; // null if adding, non-null if editing
        public Product currSelectedProduct; // null if adding, non-null if editing

        public  Sale_Order currSelOrder; // null if adding, non-null if editing

        private void AddOrder_Click(object sender, RoutedEventArgs e)
        {
            AddEditSaleOrders dlg = new AddEditSaleOrders();
            dlg.Owner = this;

            if (dlg.ShowDialog() == true)
            {
                Sale_Order currSelOrder = (Sale_Order)lvSalesOrder.SelectedItem;
                currSelectedProduct = dlg.lvProducts.SelectedItem as Product;
                currSelectedCustomer = dlg.lvCustomers.SelectedItem as Customer;


                if (currSelOrder == null)   // If add new sale order
                {

                    try
                    {
                        if (!int.TryParse(dlg.tbQuantitySales.Text, out int SaleQuantity))
                        {
                            MessageBox.Show("Quantity must be an integer");
                            return;
                        }

                        decimal costPerUnit = dlg.currSelectedProduct.SellingPrice;
                        decimal totalPrice = SaleQuantity * costPerUnit;

                        Sale_Order s = new Sale_Order
                        {

                            ProductID = currSelectedProduct.ProductID,
                            CustomersID = currSelectedCustomer.CustomersID,
                            Description = currSelectedProduct.Description,
                            SaleQuantity = SaleQuantity,
                            UnitOfMeasure = currSelectedProduct.UnitOfMeasure,
                            SellingPrice = totalPrice, // currSelectedProduct.SellingPrice * costPerUnit,
                            ShipMethod = dlg.cbShippingSales.Text,
                            OrderDate = (DateTime)dlg.dpOrderDateSales.SelectedDate,
                            ShipDate = (DateTime)dlg.dpShipDateSales.SelectedDate,
                            OrderStatus = dlg.cbStatusSales.Text
                        };
                        ctx.Sale_Order.Add(s);
                        ctx.SaveChanges();
                        lvSalesOrder.ItemsSource = (from p in ctx.Sale_Order select p).ToList<Sale_Order>();
                    }
                    catch (SystemException ex)
                    {
                        MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                    }
                }
                else    // if edit existing order
                {
                    return;
                }

            }
        }

        private void Cancel_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void DeleteOrder_Click(object sender, RoutedEventArgs e)
        {
            Sale_Order currSelOrder = (Sale_Order)lvSalesOrder.SelectedItem;
            if (currSelOrder == null) { return; } // should never happen
            if (MessageBoxResult.OK != MessageBox.Show("Do you want to delete the order?\n " + currSelOrder, "Warning", MessageBoxButton.OKCancel, MessageBoxImage.Warning))
            { return; } // cancelled
            try
            {

                ctx.Sale_Order.Remove(currSelOrder);
                ctx.SaveChanges();
                lvSalesOrder.ItemsSource = (from p in ctx.Sale_Order select p).ToList<Sale_Order>();
                lvSalesOrder.Items.Refresh();
                btAddEditOrder.IsEnabled = true;
                btDeleteOrder.IsEnabled = false;

            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
        }

        private void lvSalesOrder_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Sale_Order currSelOrder = (Sale_Order)lvSalesOrder.SelectedItem;
            
            if (currSelOrder == null)
            {
                return;
            }
            else
            {
                btAddEditOrder.IsEnabled = false;
                btDeleteOrder.IsEnabled = true;
            }
        }

        private void btSaveSelected_Click(object sender, RoutedEventArgs e)
        {
            var selItems = lvSalesOrder.SelectedItems.Cast<Sale_Order>().ToList();
            if (selItems.Count == 0)
            {
                MessageBox.Show(this, "Please select some items first", "Input error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.Filter = "CSV files (*.csv)|*.csv|All files (*.*)|*.*";
            saveFileDialog.FilterIndex = 1;
            
            if (saveFileDialog.ShowDialog() == true)
            {
                string fileName = saveFileDialog.FileName;
                SaveDataToFile(fileName, selItems);
            }
        }

        private void SaveDataToFile(string fileName, List<Sale_Order> tripsToSave)
        {
            try
            {
                List<string> linesList = new List<string>();
                foreach (Sale_Order t in tripsToSave)
                {
                    linesList.Add(t.ToDataString());
                }
                File.WriteAllLines(fileName, linesList);
            }
            catch (Exception ex) when (ex is IOException || ex is SystemException)
            {
                MessageBox.Show(this, ex.Message, "File IO error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

    }
}
