﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WarehouseMS
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //warehouseEntities ctx;
        public MainWindow()
        {
            InitializeComponent();
            //ctx = new warehouseEntities();
            
        }


        private void Purchases_Click(object sender, RoutedEventArgs e)
        {
            Purchases dlg = new Purchases();
            dlg.Owner = this;
            dlg.ShowDialog();
        }

        private void Sale_Click(object sender, RoutedEventArgs e)
        {
            Sales dlg = new Sales();
            dlg.Owner = this;
            dlg.ShowDialog();
        }

        private void Vendor_Click(object sender, RoutedEventArgs e)
        {
            Vendors dlg = new Vendors();
            dlg.Owner = this;
            dlg.ShowDialog();
        }

        private void Customer_Click(object sender, RoutedEventArgs e)
        {
            Customers dlg = new Customers();
            dlg.Owner = this;
            dlg.ShowDialog();
        }

        private void Inventory_Click(object sender, RoutedEventArgs e)
        {
            Inventories dlg = new Inventories();
            dlg.Owner = this;
            dlg.ShowDialog();
        }

        private void Product_Click(object sender, RoutedEventArgs e)
        {
            Products dlg = new Products();
            dlg.Owner = this;
            dlg.ShowDialog();
        }

        private void btReports_Click(object sender, RoutedEventArgs e)
        {
            ReportsWindow dlg = new ReportsWindow();
            dlg.Owner = this;
            dlg.ShowDialog();
        }

        private void report_Click(object sender, RoutedEventArgs e)
        {
            ReportsWindow dlg = new ReportsWindow();
            dlg.Owner = this;
            dlg.ShowDialog();
        }
    }
}
